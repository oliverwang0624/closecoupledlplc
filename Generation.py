# -*- coding: utf-8 -*-

#This is a script for training the SVR model of 
#the LPLCs of closely connected bends


from sklearn.preprocessing import RobustScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import MaxAbsScaler
from sklearn.preprocessing import QuantileTransformer
from sklearn.svm import SVR
import numpy as np
import pandas as pd
import os 
import scipy.io as sio



if __name__ == "__main__":
    repoPath = os.getcwd()
    np.set_printoptions(precision=3)
    writer = pd.ExcelWriter(repoPath+'/ModelData.xlsx', engine = 'xlsxwriter')
#    #############################################
    #D&D model generation
    ##Read input and output data
#    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'D&D')
#    x = originalData[['Opening_up','opening_down','InletV/D','LinB/D']]
#    y_ori = originalData['LPLC_dec']
#    y = np.cbrt(originalData['LPLC_dec'])
#    
#    # Pre-processing of normalization
#    scaler = RobustScaler(quantile_range=(25, 75))
#    x_normal = scaler.fit_transform(x)
#    
#    #Read svr parameters
#    f1 = open('D&D.txt','r')
#    lines = f1.readlines()
#    f1.close()
#    C = float(lines[0].replace('[','').split()[0])
#    epsilon = 0.001
#    gamma = float(lines[0].replace('[','').split()[1])
#    
#    # Model training
#    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
#    svr.fit(x_normal,y)
#    y_predict = np.power(svr.predict(x_normal),3)
#    
#    testData = pd.read_excel(repoPath+'/DataGeneration.xlsx',sheet_name = 'D&D')
#    x_test = testData[['opening_up','opening_down','InletV/D','LinB/D']]
#    x_test_normalized = scaler.transform(x_test)
#    y_test = np.power(svr.predict(x_test_normalized),3)
#    LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
#    testData =pd.concat([testData,LPLCDP],axis = 1)
#    testData.index = testData.index+1
#    testData.to_excel(writer,sheet_name = 'D&D')
#    data = []
#    for i in range(6):
#        shaped = np.zeros([7,7])
#        shaped[1:,0] = np.unique(testData['InletV/D'].values)
#        shaped[0,1:] = np.unique(testData['opening_up'].values)
#        for j in range(6):
#            shaped[1:,j+1] = testData['LPLC'].values[36*i+6*j:36*i+6*(j+1)]
#        data.append(shaped)
#    sio.savemat(repoPath+'/DD.mat',{'Dam30':data[0],'Dam42':data[1],'Dam54':data[2],'Dam66':data[3],'Dam78':data[4],'Dam90':data[5]})     
#    ##############################################
#    #B&J_str model generation
#    ##Read input and output data
#    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'B&J_str')
#    x = originalData[['InletV/D','LinB/D','AreaRatio','FlowRatio']]
#    y_ori = originalData['strLPLC_dec']
#    y = np.cbrt(originalData['strLPLC_dec'])
#    
#    # Pre-processing of normalization
#    scaler = RobustScaler(quantile_range=(25, 75))
#    x_normal = scaler.fit_transform(x)
#    
#    #Read svr parameters
#    f1 = open('B&J_str.txt','r')
#    lines = f1.readlines()
#    f1.close()
#    C = float(lines[0].replace('[','').split()[0])
#    epsilon = 0.001
#    gamma = float(lines[0].replace('[','').split()[1])
#    
#    # Model training
#    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
#    svr.fit(x_normal,y)
#    y_predict = np.power(svr.predict(x_normal),3)
#    
#    testData = pd.read_excel(repoPath+'/DataGeneration.xlsx',sheet_name = 'B&J_str')
#    x_test = testData[['InletV/D','LinB/D','AreaRatio','FlowRatio']]
#    x_test_normalized = scaler.transform(x_test)
#    y_test = np.power(svr.predict(x_test_normalized),3)
#    LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
#    testData =pd.concat([testData,LPLCDP],axis = 1)
#    testData.index = testData.index+1
#    testData.to_excel(writer,sheet_name = 'B&J_str')
#    shaped = np.zeros([7,7])
#    shaped[1:,0] = np.unique(testData['InletV/D'].values)
#    shaped[0,1:] = np.unique(testData['FlowRatio'].values)
#    for i in range(6):
#        shaped[1:,i+1] = testData['LPLC'].values[6*i:6*(i+1)]
#    sio.savemat(repoPath+'/B&J_str.mat',{'BJ_str':shaped[:,:]})   
#    ##############################################
#    #B&J_ver model generation
#    ##Read input and output data
#    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'B&J_ver')
#    x = originalData[['InletV/D','LinB/D','AreaRatio','FlowRatio']]
#    y_ori = originalData['verLPLC_dec']
#    y = y_ori
#    
#    # Pre-processing of normalization
#    scaler = RobustScaler(quantile_range=(25, 75))
#    x_normal = scaler.fit_transform(x)
#    
#    #Read svr parameters
#    f1 = open('B&J_ver.txt','r')
#    lines = f1.readlines()
#    f1.close()
#    C = float(lines[0].replace('[','').split()[0])
#    epsilon = 0.001
#    gamma = float(lines[0].replace('[','').split()[1])
#    
#    # Model training
#    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
#    svr.fit(x_normal,y)
#    y_predict = svr.predict(x_normal)
#    
#    testData = pd.read_excel(repoPath+'/DataGeneration.xlsx',sheet_name = 'B&J_ver')
#    x_test = testData[['InletV/D','LinB/D','AreaRatio','FlowRatio']]
#    x_test_normalized = scaler.transform(x_test)
#    y_test = svr.predict(x_test_normalized)
#    LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
#    testData =pd.concat([testData,LPLCDP],axis = 1)
#    testData.index = testData.index+1
#    testData.to_excel(writer,sheet_name = 'B&J_ver')
#    shaped = np.zeros([7,7])
#    shaped[1:,0] = np.unique(testData['InletV/D'].values)
#    shaped[0,1:] = np.unique(testData['FlowRatio'].values)
#    for i in range(6):
#        shaped[1:,i+1] = testData['LPLC'].values[6*i:6*(i+1)]
#    sio.savemat(repoPath+'/B&J_ver.mat',{'BJ_ver':shaped[:,:]})          
#    ##############################################
#    #D&B model generation
#    ##Read input and output data
#    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'D&B')
#    x = originalData[['InletV/D','opening','LinB/D']]
#    y_ori = originalData['Bend_dec']
#    y = np.cbrt(originalData['Bend_dec'])
#    
#    # Pre-processing of normalization
#    scaler = RobustScaler(quantile_range=(25, 75))
#    x_normal = scaler.fit_transform(x)
#    
#    #Read svr parameters
#    f1 = open('D&B.txt','r')
#    lines = f1.readlines()
#    f1.close()
#    C = float(lines[0].replace('[','').split()[0])
#    epsilon = 0.001
#    gamma = float(lines[0].replace('[','').split()[1])
##    w1 = float(lines[0].replace('[','').split()[2])
##    w2 = float(lines[0].replace('[','').split()[3])
##    w3 = float(lines[0].replace('[','').split()[4])
#    
#    # Model training
#    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
##    svr.fit(x_normal*[w1,w2,w3],y)
##    y_predict = svr.predict(x_normal*[w1,w2,w3])
#    svr.fit(x_normal,y)
#    y_predict = np.power(svr.predict(x_normal),3)
#    
#    testData = pd.read_excel(repoPath+'/DataGeneration.xlsx',sheet_name = 'D&B')
#    x_test = testData[['InletV/D','opening','LinB/D']]
#    x_test_normalized = scaler.transform(x_test)
##    y_test = svr.predict(x_test_normalized*[w1,w2,w3])
#    y_test = np.power(svr.predict(x_test_normalized),3)
#    LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
#    testData =pd.concat([testData,LPLCDP],axis = 1)
#    testData.index = testData.index+1
#    testData.to_excel(writer,sheet_name = 'D&B')
#    shaped = np.zeros([7,7])
#    shaped[1:,0] = np.unique(testData['InletV/D'].values)
#    shaped[0,1:] = np.unique(testData['opening'].values)
#    for i in range(6):
#        shaped[i+1,1:] = testData['LPLC'].values[6*i:6*(i+1)]
#    sio.savemat(repoPath+'/DB.mat',{'DB':shaped[:,:]})      
##    ##############################################
#    #D&J_str model generation
#    ##Read input and output data
#    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'D&J_str')
#    x = originalData[['InletV/D','opening','LinB/D','FlowRatio','AreaRatio']]
#    y_ori = originalData['strLPLC_dec']
#    y = np.cbrt(originalData['strLPLC_dec'])
#    
#    # Pre-processing of normalization
#    scaler = RobustScaler(quantile_range=(25, 75))
#    x_normal = scaler.fit_transform(x)
#    
#    #Read svr parameters
#    f1 = open('D&J_str.txt','r')
#    lines = f1.readlines()
#    f1.close()
#    C = float(lines[0].replace('[','').split()[0])
#    epsilon = 0.001
#    gamma = float(lines[0].replace('[','').split()[1])
#    
#    # Model training
#    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
#    svr.fit(x_normal,y)
#    y_predict = np.power(svr.predict(x_normal),3)
#    
#    testData = pd.read_excel(repoPath+'/DataGeneration.xlsx',sheet_name = 'D&J_str')
#    x_test = testData[['InletV/D','opening','LinB/D','FlowRatio','AreaRatio']]
#    x_test_normalized = scaler.transform(x_test)
#    y_test = np.power(svr.predict(x_test_normalized),3)
#    LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
#    testData =pd.concat([testData,LPLCDP],axis = 1)
#    testData.index = testData.index+1
#    testData.to_excel(writer,sheet_name = 'D&J_str')
#    data = []
#    for i in range(6):
#        shaped = np.zeros([7,7])
#        shaped[1:,0] = np.unique(testData['InletV/D'].values)
#        shaped[0,1:] = np.unique(testData['FlowRatio'].values)
#        for j in range(6):
#            shaped[1:,j+1] = testData['LPLC'].values[36*i+6*j:36*i+6*(j+1)]
#        data.append(shaped)
#    sio.savemat(repoPath+'/DJ_str.mat',{'Dam30':data[0],'Dam42':data[1],'Dam54':data[2],'Dam66':data[3],'Dam78':data[4],'Dam90':data[5]})     
#     ##############################################
#    #D&J_ver model generation
#    ##Read input and output data
#    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'D&J_ver')
#    x = originalData[['InletV/D','opening','LinB/D','FlowRatio','AreaRatio']]
#    y_ori = originalData['verLPLC_dec']
#    y = np.cbrt(originalData['verLPLC_dec'])
#    
#    # Pre-processing of normalization
#    scaler = RobustScaler(quantile_range=(25, 75))
#    x_normal = scaler.fit_transform(x)
#    
#    #Read svr parameters
#    f1 = open('D&J_ver.txt','r')
#    lines = f1.readlines()
#    f1.close()
#    C = float(lines[0].replace('[','').split()[0])
#    epsilon = 0.001
#    gamma = float(lines[0].replace('[','').split()[1])
#    
#    # Model training
#    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
#    svr.fit(x_normal,y)
#    y_predict = np.power(svr.predict(x_normal),3)
#    
#    testData = pd.read_excel(repoPath+'/DataGeneration.xlsx',sheet_name = 'D&J_ver')
#    x_test = testData[['InletV/D','opening','LinB/D','FlowRatio','AreaRatio']]
#    x_test_normalized = scaler.transform(x_test)
#    y_test = np.power(svr.predict(x_test_normalized),3)
#    LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
#    testData =pd.concat([testData,LPLCDP],axis = 1)
#    testData.index = testData.index+1
#    testData.to_excel(writer,sheet_name = 'D&J_ver')
#    data = []
#    for i in range(6):
#        shaped = np.zeros([7,7])
#        shaped[1:,0] = np.unique(testData['InletV/D'].values)
#        shaped[0,1:] = np.unique(testData['FlowRatio'].values)
#        for j in range(6):
#            shaped[1:,j+1] = testData['LPLC'].values[36*i+6*j:36*i+6*(j+1)]
#        data.append(shaped)
#    sio.savemat(repoPath+'/DJ_ver.mat',{'Dam30':data[0],'Dam42':data[1],'Dam54':data[2],'Dam66':data[3],'Dam78':data[4],'Dam90':data[5]})     
##      
#    ##############################################
#    #J&D_str model generation
#    ##Read input and output data
#    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'J&D')
#    x = originalData[['inletV/D','AreaRatio','FlowRatio','LinB/D','opening']]
#    y_ori = originalData['LPLC_dec']
#    y = np.cbrt(originalData['LPLC_dec'])
#    
#    # Pre-processing of normalization
#    scaler = RobustScaler(quantile_range=(25, 75))
#    x_normal = scaler.fit_transform(x)
#    
#    #Read svr parameters
#    f1 = open('J&D.txt','r')
#    lines = f1.readlines()
#    f1.close()
#    C = float(lines[0].replace('[','').split()[0])
#    epsilon = 0.001
#    gamma = float(lines[0].replace('[','').split()[1])
#    
#    # Model training
#    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
#    svr.fit(x_normal,y)
#    y_predict = np.power(svr.predict(x_normal),3)
#    
#    testData = pd.read_excel(repoPath+'/DataGeneration.xlsx',sheet_name = 'J_str&D')
#    x_test = testData[['InletV/D','AreaRatio','FlowRatio','strLinB/D','opening']]
#    x_test_normalized = scaler.transform(x_test)
#    y_test = np.power(svr.predict(x_test_normalized),3)
#    LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
#    testData =pd.concat([testData,LPLCDP],axis = 1)
#    testData.index = testData.index+1
#    testData.to_excel(writer,sheet_name = 'J_str&D')
#    data = []
#    for i in range(6):
#        shaped = np.zeros([7,7])
#        shaped[1:,0] = np.unique(testData['InletV/D'].values)
#        shaped[0,1:] = np.unique(testData['FlowRatio'].values)
#        for j in range(6):
#            shaped[1:,j+1] = testData['LPLC'].values[36*i+6*j:36*i+6*(j+1)]
#        data.append(shaped)
#    sio.savemat(repoPath+'/J_strD.mat',{'Dam30':data[0],'Dam44':data[1],'Dam58':data[2],'Dam72':data[3],'Dam86':data[4],'Dam100':data[5]})     
#    ##############################################
#    #J&D_ver model generation
#    ##Read input and output data
#    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'J_ver&D')
#    x = originalData[['inletV/D','AreaRatio','FlowRatio','LinB/D','opening']]
#    y_ori = originalData['verLPLC_dec']
#    y = np.cbrt(originalData['verLPLC_dec'])
#    
#    # Pre-processing of normalization
#    scaler = RobustScaler(quantile_range=(25, 75))
#    x_normal = scaler.fit_transform(x)
#    
#    #Read svr parameters
#    f1 = open('J&D_ver.txt','r')
#    lines = f1.readlines()
#    f1.close()
#    C = float(lines[0].replace('[','').split()[0])
#    epsilon = 0.001
#    gamma = float(lines[0].replace('[','').split()[1])
#    
#    # Model training
#    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
#    svr.fit(x_normal,y)
#    y_predict = np.power(svr.predict(x_normal),3)
#    
#    testData = pd.read_excel(repoPath+'/DataGeneration.xlsx',sheet_name = 'J_ver&D')
#    x_test = testData[['InletV/D','AreaRatio','FlowRatio','verLinB/D','opening']]
#    x_test_normalized = scaler.transform(x_test)
#    y_test = np.power(svr.predict(x_test_normalized),3)
#    LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
#    testData =pd.concat([testData,LPLCDP],axis = 1)
#    testData.index = testData.index+1
#    testData.to_excel(writer,sheet_name = 'J_ver&D')
#    data = []
#    for i in range(6):
#        shaped = np.zeros([7,7])
#        shaped[1:,0] = np.unique(testData['InletV/D'].values)
#        shaped[0,1:] = np.unique(testData['FlowRatio'].values)
#        for j in range(6):
#            shaped[1:,j+1] = testData['LPLC'].values[36*i+6*j:36*i+6*(j+1)]
#        data.append(shaped)
#    sio.savemat(repoPath+'/J_verD.mat',{'Dam30':data[0],'Dam44':data[1],'Dam58':data[2],'Dam72':data[3],'Dam86':data[4],'Dam100':data[5]})     
#    ##############################################
#    #J&J_str model generation
#    ##Read input and output data
#    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'J_str&J_str')
#    x = originalData[['inletV/D','AreaRatio1','AreaRatio2','FlowRatio1','FlowRatio2','LinB/D']]
#    y_ori = originalData['strLPLC_dec']
#    y = originalData['strLPLC_dec']
#    
#    # Pre-processing of normalization
#    scaler = RobustScaler(quantile_range=(25, 75))
#    x_normal = scaler.fit_transform(x)
#    
#    #Read svr parameters
#    f1 = open('J&J_str.txt','r')
#    lines = f1.readlines()
#    f1.close()
#    C = float(lines[0].replace('[','').split()[0])
#    epsilon = 0.001
#    gamma = float(lines[0].replace('[','').split()[1])
#    
#    # Model training
#    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
#    svr.fit(x_normal,y)
#    y_predict =svr.predict(x_normal)
#    
#    testData = pd.read_excel(repoPath+'/DataGeneration.xlsx',sheet_name = 'J_str&J_str')
#    x_test = testData[['InletV/D','AreaRatio1','AreaRatio2','FlowRatio1','FlowRatio2','LinB/D']]
#    x_test_normalized = scaler.transform(x_test)
#    y_test = svr.predict(x_test_normalized)
#    LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
#    testData =pd.concat([testData,LPLCDP],axis = 1)
#    testData.index = testData.index+1
#    testData.to_excel(writer,sheet_name = 'J_str&J_str')
#    data = []
#    for i in range(6):
#        shaped = np.zeros([7,7])
#        shaped[1:,0] = np.unique(testData['InletV/D'].values)
#        shaped[0,1:] = np.unique(testData['FlowRatio2'].values)
#        for j in range(6):
#            shaped[1:,j+1] = testData['LPLC'].values[36*i+6*j:36*i+6*(j+1)]
#        data.append(shaped)
#    sio.savemat(repoPath+'/J_strJ_str.mat',{'FR05':data[0],'FR058':data[1],'FR066':data[2],'FR074':data[3],'FR082':data[4],'FR09':data[5]})     
#     #############################################
#    #J&J_ver model generation
#    ##Read input and output data
#    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'J_str&J_ver')
#    x = originalData[['inletV/D','AreaRatio1','AreaRatio2','FlowRatio1','FlowRatio2','LinB/D']]
#    y_ori = originalData['verLPLC_dec']
#    y = originalData['verLPLC_dec']
#    
#    # Pre-processing of normalization
#    scaler = RobustScaler(quantile_range=(25, 75))
#    x_normal = scaler.fit_transform(x)
#    
#    #Read svr parameters
#    f1 = open('J&J_ver.txt','r')
#    lines = f1.readlines()
#    f1.close()
#    C = float(lines[0].replace('[','').split()[0])
#    epsilon = 0.001
#    gamma = float(lines[0].replace('[','').split()[1])
#    
#    # Model training
#    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
#    svr.fit(x_normal,y)
#    y_predict = svr.predict(x_normal)
#    
#    testData = pd.read_excel(repoPath+'/DataGeneration.xlsx',sheet_name = 'J_str&J_ver')
#    x_test = testData[['InletV/D','AreaRatio1','AreaRatio2','FlowRatio1','FlowRatio2','LinB/D']]
#    x_test_normalized = scaler.transform(x_test)
#    y_test = svr.predict(x_test_normalized)
#    LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
#    testData =pd.concat([testData,LPLCDP],axis = 1)
#    testData.index = testData.index+1
#    testData.to_excel(writer,sheet_name = 'J_str&J_ver')
#    data = []
#    for i in range(6):
#        shaped = np.zeros([7,7])
#        shaped[1:,0] = np.unique(testData['InletV/D'].values)
#        shaped[0,1:] = np.unique(testData['FlowRatio2'].values)
#        for j in range(6):
#            shaped[1:,j+1] = testData['LPLC'].values[36*i+6*j:36*i+6*(j+1)]
#        data.append(shaped)
#    sio.savemat(repoPath+'/J_strJ_ver.mat',{'FR05':data[0],'FR058':data[1],'FR066':data[2],'FR074':data[3],'FR082':data[4],'FR09':data[5]})     
##    ##############################################
#    IndiJun_str model generation
    #Read input and output data
#    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'Indi_Jun')
#    x = originalData[['InletV/D','FlowRatio_s','AreaRatio_s']]
#    y_ori = originalData['strLPLC']
#    y = np.cbrt(originalData['strLPLC'])
##    y = y_ori
#    
#    # Pre-processing of normalization
#    scaler = RobustScaler(quantile_range=(25, 75))
##    scaler = MinMaxScaler()
##    scaler = MaxAbsScaler()
##    scaler=QuantileTransformer(output_distribution='normal')
#    x_normal = scaler.fit_transform(x)
#    
#    #Read svr parameters
#    f1 = open('Indi_Jun_str.txt','r')
#    lines = f1.readlines()
#    f1.close()
#    C = float(lines[0].replace('[','').split()[0])
#    epsilon = 0.001
#    gamma = float(lines[0].replace('[','').split()[1])
#    
#    
#    # Model training
#    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
#    svr.fit(x_normal,y)
#    y_predict = np.power(svr.predict(x_normal),3)
##    y_predict = svr.predict(x_normal)
#    
#    testData = pd.read_excel(repoPath+'/DataGeneration.xlsx',sheet_name = 'J&J')
#    x_test = testData[['InletV/D','FlowRatio_s','AreaRatio_s']]
#    x_test_normalized = scaler.transform(x_test)
#    y_test = np.power(svr.predict(x_test_normalized),3)
##    y_test = svr.predict(x_test_normalized)
#    LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
#    testData =pd.concat([testData,LPLCDP],axis = 1)
#    testData.index = testData.index+1
#    testData.to_excel(writer,sheet_name = 'IndiJun_str')
#    data = []
#    for i in range(6):
#        shaped = np.zeros([7,7])
#        shaped[1:,0] = np.unique(testData['InletV/D'].values)
#        shaped[0,1:] = np.unique(testData['FlowRatio'].values)
#        for j in range(6):
#            shaped[1:,j+1] = testData['LPLC'].values[36*i+6*j:36*i+6*(j+1)]
#        data.append(shaped)
#    sio.savemat(repoPath+'/DJ_str.mat',{'Dam30':data[0],'Dam42':data[1],'Dam54':data[2],'Dam66':data[3],'Dam78':data[4],'Dam90':data[5]})     
##########################################################
#    IndiJun_ver model generation
    #Read input and output data
#    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'Indi_Jun')
#    x = originalData[['InletV/D','FlowRatio_v','AreaRatio_v']]
#    y_ori = originalData['verLPLC']
#    y = np.cbrt(originalData['verLPLC'])
##    y = y_ori
#    
#    # Pre-processing of normalization
#    scaler = RobustScaler(quantile_range=(25, 75))
##    scaler = MinMaxScaler()
##    scaler = MaxAbsScaler()
##    scaler=QuantileTransformer(output_distribution='normal')
#    x_normal = scaler.fit_transform(x)
#    
#    #Read svr parameters
#    f1 = open('Indi_Jun_ver.txt','r')
#    lines = f1.readlines()
#    f1.close()
#    C = float(lines[0].replace('[','').split()[0])
#    epsilon = 0.001
#    gamma = float(lines[0].replace('[','').split()[1])
#    
#    
#    # Model training
#    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
#    svr.fit(x_normal,y)
#    y_predict = np.power(svr.predict(x_normal),3)
##    y_predict = svr.predict(x_normal)
#    
#    testData = pd.read_excel(repoPath+'/DataGeneration.xlsx',sheet_name = 'D&J')
#    x_test = testData[['InletV/D','FlowRatio_v','AreaRatio_v']]
#    x_test_normalized = scaler.transform(x_test)
#    y_test = np.power(svr.predict(x_test_normalized),3)
##    y_test = svr.predict(x_test_normalized)
#    LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
#    testData =pd.concat([testData,LPLCDP],axis = 1)
#    testData.index = testData.index+1
#    testData.to_excel(writer,sheet_name = 'IndiJun_ver')
#    data = []
#    for i in range(6):
#        shaped = np.zeros([7,7])
#        shaped[1:,0] = np.unique(testData['InletV/D'].values)
#        shaped[0,1:] = np.unique(testData['FlowRatio'].values)
#        for j in range(6):
#            shaped[1:,j+1] = testData['LPLC'].values[36*i+6*j:36*i+6*(j+1)]
#        data.append(shaped)
#    sio.savemat(repoPath+'/DJ_str.mat',{'Dam30':data[0],'Dam42':data[1],'Dam54':data[2],'Dam66':data[3],'Dam78':data[4],'Dam90':data[5]})     
#######################################################
    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'Indi_D')
    x = originalData[['opening','InletV/D']]
    y_ori = originalData['LPLC']
    y = np.cbrt(originalData['LPLC'])
#    y = y_ori
    
    # Pre-processing of normalization
    scaler = RobustScaler(quantile_range=(25, 75))
#    scaler = MinMaxScaler()
#    scaler = MaxAbsScaler()
#    scaler=QuantileTransformer(output_distribution='normal')
    x_normal = scaler.fit_transform(x)
    
    #Read svr parameters
    f1 = open('Indi_D.txt','r')
    lines = f1.readlines()
    f1.close()
    C = float(lines[0].replace('[','').split()[0])
    epsilon = 0.001
    gamma = float(lines[0].replace('[','').split()[1])
    
    
    # Model training
    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
    svr.fit(x_normal,y)
    y_predict = np.power(svr.predict(x_normal),3)
#    y_predict = svr.predict(x_normal)
    
    testData = pd.read_excel(repoPath+'/DataGeneration.xlsx',sheet_name = 'Indi_Dam')
    x_test = testData[['opening','InletV/D']]
    x_test_normalized = scaler.transform(x_test)
    y_test = np.power(svr.predict(x_test_normalized),3)
#    y_test = svr.predict(x_test_normalized)
    LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
    testData =pd.concat([testData,LPLCDP],axis = 1)
    testData.index = testData.index+1
    testData.to_excel(writer,sheet_name = 'Indi_D')
    shaped = np.zeros([6,6])
    shaped[1:,0] = np.unique(testData['InletV/D'].values)
    shaped[0,1:] = np.unique(testData['opening'].values)
    for i in range(5):
        shaped[i+1,1:] = testData['LPLC'].values[5*i:5*(i+1)]
    sio.savemat(repoPath+'/Damper.mat',{'Damper':shaped[:,:]})   
         
    
    
    
    
    writer.save()
    writer.close()
    
    
    
    
    