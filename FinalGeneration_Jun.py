# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 10:34:08 2019

@author: olive
"""

from sklearn.preprocessing import RobustScaler
from sklearn.svm import SVR
import numpy as np
import pandas as pd
import os 
import scipy.io as sio

if __name__ == "__main__":
    repoPath = os.getcwd()
    np.set_printoptions(precision=3)
    
    
    ##Set inlet velocity over diameter
    massFlow = [0.2,0.3,0.8,0.5,0.4,0.3,0.2]
    Diameter = [0.25,0.35,0.4,0.35,0.35,0.35,0.25]
    InletVD = np.zeros_like(Diameter)
    for i in range(7):
        InletVD[i] = massFlow[i]/1.2/(0.25*np.pi*np.square(Diameter[i]))/Diameter[i]
    for k in range(7):
        writer = pd.ExcelWriter(repoPath+'/ModelData'+str(k+1)+'.xlsx', engine = 'xlsxwriter')
        ##Set Area ratio and mass flow ratio matrix
        #Straight
        AreaRatio_str = np.arange(5,11)/10
        MasFloRatio_str= np.arange(0.5,1,0.1)
        Matrix = np.zeros([np.size(AreaRatio_str)*np.size(MasFloRatio_str),2])
        for i in range(np.size(AreaRatio_str)):
            for j in range(np.size(MasFloRatio_str)):
                Matrix[i*np.size(MasFloRatio_str)+j,0]=AreaRatio_str[i]
                Matrix[i*np.size(MasFloRatio_str)+j,1] = MasFloRatio_str[j]
        MatrixDF_str = pd.DataFrame(Matrix,columns=["AreaRatio","FlowRatio"])
        InletVDDF = pd.DataFrame(np.ones([np.size(AreaRatio_str)*np.size(MasFloRatio_str),1])*InletVD[k],columns=["InletVD"])
        MatrixDF_str = pd.concat([InletVDDF,MatrixDF_str],axis=1)
        #Vertical
        AreaRatio_ver = 1.15-np.arange(5,11)/10
        MasFloRatio_ver= 1-np.arange(0.5,1,0.1)
        Matrix = np.zeros([np.size(AreaRatio_ver)*np.size(MasFloRatio_ver),2])
        for i in range(np.size(AreaRatio_str)):
            for j in range(np.size(MasFloRatio_ver)):
                Matrix[i*np.size(MasFloRatio_ver)+j,0]=AreaRatio_ver[i]
                Matrix[i*np.size(MasFloRatio_ver)+j,1] = MasFloRatio_ver[j]
        MatrixDF_ver = pd.DataFrame(Matrix,columns=["AreaRatio","FlowRatio"])
        InletVDDF = pd.DataFrame(np.ones([np.size(AreaRatio_ver)*np.size(MasFloRatio_ver),1])*InletVD[k],columns=["InletVD"])
        MatrixDF_ver = pd.concat([InletVDDF,MatrixDF_ver],axis=1)
        MatrixDF_str = MatrixDF_str.sort_values(by=["AreaRatio","FlowRatio"])
        MatrixDF_str.reset_index(drop=True, inplace=True)
        MatrixDF_ver = MatrixDF_ver.sort_values(by=["AreaRatio","FlowRatio"])
        MatrixDF_ver.reset_index(drop=True, inplace=True)
        ##Calculate LPLC
        ##Straight branch
        #Read input and output data
        originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'Indi_Jun')
        x = originalData[['InletV/D','FlowRatio_s','AreaRatio_s']]
        y_ori = originalData['strLPLC']
        y = np.cbrt(originalData['strLPLC'])
    
        # Pre-processing of normalization
        scaler = RobustScaler(quantile_range=(25, 75))
        x_normal = scaler.fit_transform(x)
        
        #Read svr parameters
        f1 = open('Indi_Jun_str.txt','r')
        lines = f1.readlines()
        f1.close()
        C = float(lines[0].replace('[','').split()[0])
        epsilon = 0.001
        gamma = float(lines[0].replace('[','').split()[1])
        
        # Model training
        svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
        svr.fit(x_normal,y)
        y_predict = np.power(svr.predict(x_normal),3)
    
        testData = MatrixDF_str
        x_test = testData[['InletVD','FlowRatio','AreaRatio']]
        x_test_normalized = scaler.transform(x_test)
        y_test = np.power(svr.predict(x_test_normalized),3)
        LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
        testData =pd.concat([testData,LPLCDP],axis = 1)
        testData.index = testData.index+1
        testData.to_excel(writer,sheet_name = 'IndiJun_str')
        shaped1 = np.zeros([np.size(AreaRatio_str)+1,np.size(MasFloRatio_str)+1])
        shaped1[1:,0] = np.unique(testData['AreaRatio'].values)
        shaped1[0,1:] = np.unique(testData['FlowRatio'].values)
        for i in range(np.size(AreaRatio_str)):
            shaped1[i+1,1:] = testData['LPLC'].values[np.size(MasFloRatio_str)*i:np.size(MasFloRatio_str)*(i+1)]
        
        ##Vertical branch
        #Read input and output data
        originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'Indi_Jun')
        x = originalData[['InletV/D','FlowRatio_v','AreaRatio_v']]
        y_ori = originalData['verLPLC']
        y = np.cbrt(originalData['verLPLC'])
    
        # Pre-processing of normalization
        scaler = RobustScaler(quantile_range=(25, 75))
        x_normal = scaler.fit_transform(x)
        
        #Read svr parameters
        f1 = open('Indi_Jun_ver.txt','r')
        lines = f1.readlines()
        f1.close()
        C = float(lines[0].replace('[','').split()[0])
        epsilon = 0.001
        gamma = float(lines[0].replace('[','').split()[1])
        
        # Model training
        svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
        svr.fit(x_normal,y)
        y_predict = np.power(svr.predict(x_normal),3)
    
        testData = MatrixDF_ver
        x_test = testData[['InletVD','FlowRatio','AreaRatio']]
        x_test_normalized = scaler.transform(x_test)
        y_test = np.power(svr.predict(x_test_normalized),3)
        LPLCDP = pd.DataFrame(np.array(y_test),columns = ['LPLC'])
        testData =pd.concat([testData,LPLCDP],axis = 1)
        testData.index = testData.index+1
        testData.to_excel(writer,sheet_name = 'IndiJun_ver')
        shaped2 = np.zeros([np.size(AreaRatio_ver)+1,np.size(MasFloRatio_ver)+1])
        shaped2[1:,0] = np.unique(testData['AreaRatio'].values)
        shaped2[0,1:] = np.unique(testData['FlowRatio'].values)
        for i in range(np.size(AreaRatio_ver)):
            shaped2[i+1,1:] = testData['LPLC'].values[np.size(MasFloRatio_ver)*i:np.size(MasFloRatio_ver)*(i+1)]
    
        sio.savemat(repoPath+'/Junction'+str(k+1)+'.mat',{'Straight':shaped1[:,:],'Vertical':shaped2[:,:]})   
        
        writer.save()
        writer.close()
    
    
    
    