# -*- coding: utf-8 -*-

#This is a script for training the SVR model of 
#the LPLCs of closely connected bends


from sklearn.preprocessing import RobustScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import make_scorer
from custom_scorer_module import custom_scoring_function 
from sklearn.svm import SVR
from sklearn.model_selection import cross_val_score
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os 
from pso import pso

def kFoldCV(x,*args):

    x_train, y_train, kFold,epsilon= args
    C,gamma = x
    
    svr = SVR(C = C,epsilon = epsilon, gamma = gamma)
    score = make_scorer(custom_scoring_function, greater_is_better=False)
#    Error = cross_val_score(svr,x_train,y_train,cv=kFold,
#                                   scoring='neg_mean_absolute_error',n_jobs=-1)
    Error = cross_val_score(svr,x_train,y_train,cv=kFold,
                               scoring=score,n_jobs=-1)
    Error = -np.mean(Error)
    return Error

def newObj(x,*args):
    x_train, y_train, epsilon = args
    C, gamma = x
    
    svr = SVR(C = C,epsilon = epsilon, gamma = gamma)
    svr = svr.fit(x_train,y_train)
    score = make_scorer(custom_scoring_function, greater_is_better=True)
    
    Error = score(svr, x_train, y_train) 
    
    Obj = 0.7*Error+0.2*gamma+0.1*np.sqrt(C)
    
    return Obj

if __name__ == "__main__":
    repoPath = os.getcwd()
    
    ##Read input and output data
    originalData = pd.read_excel(repoPath+'/Summary.xlsx',sheet_name = 'J&D')
    x = originalData[['inletV/D','AreaRatio','FlowRatio','LinB/D','opening']]
    y_ori = originalData['LPLC_dec']
    y = np.cbrt(originalData['LPLC_dec'])
#    y = y_ori
    
    # Pre-processing of normalization
    scaler = RobustScaler(quantile_range=(25, 75))
    x_normal = scaler.fit_transform(x)
    
##     Parameter selection using k-fold Cross-validation
    lb = [1e-2,0.01]
    ub = [2e2,10]
    epsilon = 0.001
    kFold = 10
    args = (x_normal, y, kFold,epsilon) 
    parameter = np.zeros((1,3))
    for i in range(1):
        x, f = pso(kFoldCV,lb,ub,args=args,swarmsize=20,debug=True,maxiter=100)
        parameter[i,0] = x[0]
        parameter[i,1] = x[1]
#        parameter[i,2] = x[2]
        parameter[i,2] = f
    
    
#    C = parameter[parameter[:,3].argmin(),0]
#    epsilon = parameter[parameter[:,3].argmin(),1]
#    gamma = parameter[parameter[:,3].argmin(),2]

    C = parameter[parameter[:,2].argmin(),0]
    gamma = parameter[parameter[:,2].argmin(),1]
    
    f1 = open('J&D.txt','w+')
    f1.writelines(str(parameter[parameter[:,2].argmin(),:]))
    f1.close()
#    
    # Model training
    svr = SVR(C = C, epsilon = epsilon , gamma = gamma)
    svr.fit(x_normal,y)
    y_predict = np.power(svr.predict(x_normal),3)
#    y_predict = svr.predict(x_normal)
    
    # Prediction and verification
    fig, ax = plt.subplots(1,1)
    ax.plot(y_ori,y_predict,'o',label='Prediction')
    ax.plot(y_ori,y_ori,'-',color='k',label='GroundTruth')
    ax.plot(y_ori,y_ori*0.9,'-.',color='r',label='10% Error')
    ax.plot(y_ori,y_ori*1.1,'-.',color='r',label='')
    ax.set_xlabel('y_groundTruth')
    ax.set_ylabel('y_predicted')
    ax.legend()
    fig.savefig(repoPath+'/Regression Performance_J&D.jpg',dpi=600,format='jpg')
    
#    fig, ax = plt.subplots(1,1)
#    ax.plot(y/1000,y_predict/1000,'o',label='Prediction')
#    ax.plot(y/1000,y/1000,'-',color='k',label='GroundTruth')
#    ax.plot(y/1000,y/1000*0.9,'-.',color='r',label='10% Error')
#    ax.plot(y/1000,y/1000*1.1,'-.',color='r',label='')
#    ax.set_xlabel('y_groundTruth')
#    ax.set_ylabel('y_predicted')
#    ax.legend()
#    fig.savefig(repoPath+'/Regression Performance_D&B.jpg',dpi=600,format='jpg')
    
#    testData = pd.read_excel(repoPath+'/TrainingSet.xlsx',sheet_name = '3junctionTest')
##    x_test = testData[['LinB/D','inletV','strAreaRatio1','strAreaRatio2','strFlowRatio1','strFlowRatio2']]
#    x_test = testData[['inletV','strAreaRatio2','strFlowRatio2']]
#    x_test_normalized = scaler.transform(x_test)
##    y_test = svr.predict(x_test_normalized)/1000
#    y_test = np.exp(svr.predict(x_test_normalized)/1000)

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    