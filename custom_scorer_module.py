#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 18:06:02 2019

@author: oliverwang
"""

import numpy as np

def custom_scoring_function(y_true,y_pred):
#    maxdiff = np.abs(y_true - y_pred).max()
    meandiff = np.abs((y_true-y_pred)/y_true*100).mean()
#    maxdiff = np.abs((y_true-y_pred)/y_true*100).max()
#    meandiff = np.abs(y_true - y_pred).mean()
#    return 0.5*np.log(maxdiff)+0.5*np.log(meandiff)
#    return np.log(meandiff)
    return meandiff
#    return maxdiff