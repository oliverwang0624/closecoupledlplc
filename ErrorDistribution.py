#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 28 20:54:22 2019

@author: oliverwang
"""

import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd

path = os.getcwd()
A = pd.read_excel(path+'/ErrorResults.xlsx',sheet_name = 'J&D')
A1 = A.iloc[:,2][(A.iloc[:,2]<=np.percentile(A.iloc[:,2].values,99))
         &(A.iloc[:,2]>=np.percentile(A.iloc[:,2].values,1))].values
A2 = A.iloc[:,3][(A.iloc[:,3]<=np.percentile(A.iloc[:,3].values,99))
          &(A.iloc[:,3]>=np.percentile(A.iloc[:,3].values,1))].values
#A1=A.iloc[:,2].values
#A2=A.iloc[:,3].values
#A1 = np.cbrt(A1)
#A2 = np.cbrt(A2)
weights1 = np.ones_like(A1)/float(len(A1))
weights2 = np.ones_like(A2)/float(len(A2))
fig, ax = plt.subplots(2,1,figsize=(10,10))
ax[0].hist(A1,weights = weights1,color = 'g',edgecolor='black',linewidth=1)
ax[0].set_xlabel('strLPLCDiff',fontsize=15)
ax[1].hist(A2,weights = weights2,color = 'g',edgecolor='black',linewidth=1)
ax[1].set_xlabel('verLPLCDiff',fontsize=15)